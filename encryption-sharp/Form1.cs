﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;

namespace encryption_sharp
{
    public partial class Form1 : Form
    {
        //RSACryptoServiceProvider - класс, выполняющий ассиметричное шифрование и расшифровку с использованием алгоритма RSA
        RSACryptoServiceProvider currentRSA = new RSACryptoServiceProvider();
        //
        UnicodeEncoding byteConverter = new UnicodeEncoding();
        //
        RSAParameters rsaPublicParams;
        RSAParameters rsaPrivateParams;
        byte[] msg_output;
        byte[] signature;
        Form2 formReceiver;

        public RSAParameters RemotePublicParameters
        {
            get
            {
                return formReceiver.PublicParameters;
            }
        }

        public Form1()
        {
            InitializeComponent();
            //Генерация открытого и закрытого ключа.
            rsaPrivateParams = currentRSA.ExportParameters(true);
            rsaPublicParams = currentRSA.ExportParameters(false);
            msg_output = null;
            //Создание и отображение формы получателя 
            //без отображения иконки в панели задач
            formReceiver = new Form2();
            formReceiver.Show();
            formReceiver.ShowInTaskbar = false;
        }

        //Откроем новую форму
        private void SendMsgBtn_Click(object sender, EventArgs e)
        {
            //"Передадим" значения в переменные получателя (Form2) 
            formReceiver.msgInput = msg_output;
            formReceiver.signatureInput = signature;
            formReceiver.RemotePublicParameters = rsaPublicParams;
        }

        private void EncryptBtn_Click(object sender, EventArgs e)
        {
            //Зашифруем текст c использованием открытого ключа принимающей стороны
            //и сохраним, как массив байт
            byte[] msg_crypted = RSAEncrypt(byteConverter.GetBytes(originalTxtBox.Text), RemotePublicParameters);
            signature = HashAndSign(msg_crypted);

            //Выводим массив в второе текстовое поле, преобразуя в строку  
            //Обработаем ошибку: "Значение не может быть неопределенным"
            try
            {
                //Класс BitConverter преобразует базовые типы данных в массив байтов, а массив байтов — в базовые типы данных
                encryptTxtBox.Text = BitConverter.ToString(msg_crypted);
                //Преобразуем данные по открытым и закрытым ключам для вывода на форму
                var privateKey = currentRSA.ExportCspBlob(true);
                var publicKey = currentRSA.ExportCspBlob(false);
                string PrivateKeyOut = Convert.ToBase64String(privateKey);
                string PublicKeyOut = Convert.ToBase64String(publicKey);

                textBox1.Text = PrivateKeyOut;
                textBox2.Text = PublicKeyOut;
            }
            catch (ArgumentNullException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
            }
            msg_output = msg_crypted;
        }

        private void OriginalTxtBox_TextChanged(object sender, EventArgs e)
        {
            //Разблокируем кнопку "Зашифровать" при вводе символов в поле 1
            encryptBtn.Enabled = (originalTxtBox.Text.Length != 0);
        }

        private void EncryptTxtBox_TextChanged(object sender, EventArgs e)
        {
            //Разблокируем кнопку "Отправить" при появлении символов в поле 2
            sendMsgBtn.Enabled = (encryptTxtBox.Text.Length != 0);
        }

        //Вручную выполняет хэширование, а затем подписывает хэшированное значение.
        public byte[] HashAndSign(byte[] encrypted)
        {
            RSACryptoServiceProvider rsaCSP = new RSACryptoServiceProvider();
            //Вычисляет хэш SHA256 для входных данных 
            //Размер хеша для SHA256Managed алгоритма составляет 256 бит.
            SHA256Managed hash = new SHA256Managed();
            byte[] hashedData, byteSign;

            rsaCSP.ImportParameters(rsaPrivateParams);

            try
            {
                //Метод ComputeHash() вычисляет хэш-значение для заданного массива байтов
                hashedData = hash.ComputeHash(encrypted);
                //Выводим хэш в текстовый элемент формы
                textBox3.Text = BitConverter.ToString(hashedData);

                //Метод SignHash класса RSACryptoServiceProvider вычисляет подпись для созданного по специальному алгоритму хеша данных
                //В качестве первого параметра используется хэш-значение для заданного массива байтов
                //Алгоритм хеширования передается в качестве второго параметра в виде идентификатора, который вычисляется с помощью функции MapNameToOID(AlgorithmName)
                byteSign = rsaCSP.SignHash(hashedData, CryptoConfig.MapNameToOID("SHA256"));
            }
            //Обработка ошибок: выводим сообщение об обшибках
            catch (SystemException e)
            {
                if (e is ArgumentNullException || e is CryptographicException)
                    Console.WriteLine("Error: {0}", e.ToString());
                byteSign = null;
            }
            return byteSign;
        }

        //Метод для шифрования с использованием RSA
        public static byte[] RSAEncrypt(byte[] DataToEncrypt, RSAParameters rsaParams)
        {
            byte[] encrypted;
            try
            {
                //Создание нового экземпляра RSACryptoServiceProvider
                RSACryptoServiceProvider rsaCSP = new RSACryptoServiceProvider();

                //Импортирование информации ключа RSA. 
                //Это необходимо для примения информацию об открытом ключе.
                rsaCSP.ImportParameters(rsaParams);

                //Расшифровка переданного массива байтов, с указанием заполнением OAEP.
                //OAEP - Оптимальное асимметричное шифрование с дополнением
                //OAEP доступно только с версии MS Windows XP
                //Метод Encrypt зашифровывает данные с помощью алгоритма
                encrypted = rsaCSP.Encrypt(DataToEncrypt, false);
                return encrypted;
            }
            //Обработка ошибки: выводим сообщение о CryptographicException в консоле
            catch (CryptographicException e)
            {
                Console.WriteLine("Error: {0}", e.Message);
                return null;
            }

        }

    }
}
