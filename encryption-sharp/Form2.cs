﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;

namespace encryption_sharp
{
    public partial class Form2 : Form
    {
        //Массив для расшифрованных данных
        byte[] decryptedData;
        //Входящий массив байт от отправителя
        public byte[] msgInput { get; set; }
        //
        public byte[] signatureInput { get; set; }

        RSACryptoServiceProvider currentCSP = new RSACryptoServiceProvider();
        //Передаем открытый ключ отправителю данных
        public RSAParameters PublicParameters
        {
            get
            {
                return rsaPublicParams;
            }

        }
        //Получаем открытый ключ отправителя данных
        public RSAParameters RemotePublicParameters { get; set; }


        UnicodeEncoding byteConverter;
        RSAParameters rsaPublicParams;
        RSAParameters rsaPrivateParams;

        //private static System.Timers.Timer aTimer;

        public Form2()
        {
            InitializeComponent();
            byteConverter = new UnicodeEncoding();
            rsaPrivateParams = currentCSP.ExportParameters(true);
            rsaPublicParams = currentCSP.ExportParameters(false);

            //Запустим таймер, чтобы "обновлять" поле с зашифрованным текстом
            timer1.Enabled = true;
            timer1.Start();
            timer1.Interval = 1000;//1 sec

        }

        //Вручную выполняет хеширование, а затем проверяет хэшированное значение.
        public bool VerifyHash(RSAParameters rsaParams, byte[] signedData, byte[] signature)
        {
            RSACryptoServiceProvider rsaCSP = new RSACryptoServiceProvider();
            SHA256Managed hash = new SHA256Managed();
            byte[] hashedData;
            bool dataStatus, hashStatus;


            rsaCSP.ImportParameters(rsaParams);
            try
            {
                dataStatus = rsaCSP.VerifyData(signedData, CryptoConfig.MapNameToOID("SHA256"), signature);
            }
            //Обработка ошибки: выводим сообщение о ArgumentException или  NullReferenceException в консоле
            catch (SystemException e)
            {
                if (e is ArgumentException || e is NullReferenceException)
                {
                    Console.WriteLine("Error: {0}", e.ToString());
                    dataStatus = false;
                }
            }

            hashedData = hash.ComputeHash(signedData);
            //Выводим хэш в текстовый элемент формы
            textBox3.Text = BitConverter.ToString(hashedData);

            try
            {
                hashStatus = rsaCSP.VerifyHash(hashedData, CryptoConfig.MapNameToOID("SHA256"), signature);
            }
            //Обработка ошибки: выводим сообщение о CryptographicException в консоле ("Плохой хеш")
            //Обработка ошибки: выводим сообщение о NullReferenceException 
            //в консоле ("Ссылка на объект не указывает на экземпляр объекта")
            catch (SystemException e)
            {
                if (e is CryptographicException || e is NullReferenceException)
                    Console.WriteLine("Error: {0}", e.ToString());
                hashStatus = false;
            }

            return hashStatus;
        }

        //Расшифровка с проверкой возможности выполнения данного процесса и выводом в консоль сообщения об ошибке
        public static byte[] RSADecrypt(byte[] DataToDecrypt, RSAParameters privateParams)
        {
            byte[] decryptedData;
            string roundTrip;
            UnicodeEncoding byteConverter = new UnicodeEncoding();
            try
            {
                //Создание нового экземпляра RSACryptoServiceProvider
                RSACryptoServiceProvider rsaCSP = new RSACryptoServiceProvider();
                rsaCSP.ImportParameters(privateParams);

                //Расшифровка переданного массива байтов, с указанием заполнением OAEP.
                //OAEP - Оптимальное асимметричное шифрование с дополнением
                //OAEP доступно только с версии MS Windows XP
                decryptedData = rsaCSP.Decrypt(DataToDecrypt, false);

                roundTrip = byteConverter.GetString(decryptedData);
                Console.WriteLine("RoundTrip: {0}", roundTrip);
                return decryptedData;
            }

            //Обработка ошибки: выводим сообщение о CryptographicException в консоле
            catch (CryptographicException e)
            {
                Console.WriteLine("Error: {0}", e.ToString());
                return null;
            }

        }

        private void Timer1_Tick(object sender, EventArgs e)
        {
            if (msgInput != null)
                encryptTxtBox.Text = BitConverter.ToString(msgInput);

            Console.WriteLine("Updating...");
        }

        private void EncryptTxtBox_TextChanged(object sender, EventArgs e)
        {
            if (VerifyHash(RemotePublicParameters, msgInput, signatureInput))
            {
                decryptedData = RSADecrypt(msgInput, rsaPrivateParams);
                outputTxtBox.Text = byteConverter.GetString(decryptedData);

                //Преобразуем данные по открытым и закрытым ключам для вывода на форму
                var privateKey = currentCSP.ExportCspBlob(true);
                var publicKey = currentCSP.ExportCspBlob(false);
                string PrivateKeyOut = Convert.ToBase64String(privateKey);
                string PublicKeyOut = Convert.ToBase64String(publicKey);

                textBox1.Text = PrivateKeyOut;
                textBox2.Text = PublicKeyOut;

            }
            //Иначе выводим оповещение в консоле
            else
            {
                Console.WriteLine("Warning: Invalid signature");
            }
        }
    }
}