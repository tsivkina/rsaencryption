﻿namespace encryption_sharp
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.originalLbl = new System.Windows.Forms.Label();
            this.originalTxtBox = new System.Windows.Forms.TextBox();
            this.encryptBtn = new System.Windows.Forms.Button();
            this.encryptTxtBox = new System.Windows.Forms.TextBox();
            this.decryptLbl = new System.Windows.Forms.Label();
            this.typeEncryptLbl = new System.Windows.Forms.Label();
            this.sendMsgBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // originalLbl
            // 
            this.originalLbl.AutoSize = true;
            this.originalLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.originalLbl.Location = new System.Drawing.Point(34, 24);
            this.originalLbl.Name = "originalLbl";
            this.originalLbl.Size = new System.Drawing.Size(168, 20);
            this.originalLbl.TabIndex = 0;
            this.originalLbl.Text = "Введите сообщение:";
            // 
            // originalTxtBox
            // 
            this.originalTxtBox.Location = new System.Drawing.Point(28, 47);
            this.originalTxtBox.Multiline = true;
            this.originalTxtBox.Name = "originalTxtBox";
            this.originalTxtBox.Size = new System.Drawing.Size(629, 86);
            this.originalTxtBox.TabIndex = 1;
            this.originalTxtBox.TextChanged += new System.EventHandler(this.OriginalTxtBox_TextChanged);
            // 
            // encryptBtn
            // 
            this.encryptBtn.Enabled = false;
            this.encryptBtn.Image = ((System.Drawing.Image)(resources.GetObject("encryptBtn.Image")));
            this.encryptBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.encryptBtn.Location = new System.Drawing.Point(668, 86);
            this.encryptBtn.Name = "encryptBtn";
            this.encryptBtn.Size = new System.Drawing.Size(120, 45);
            this.encryptBtn.TabIndex = 3;
            this.encryptBtn.Text = "Зашифровать";
            this.encryptBtn.UseVisualStyleBackColor = true;
            this.encryptBtn.Click += new System.EventHandler(this.EncryptBtn_Click);
            // 
            // encryptTxtBox
            // 
            this.encryptTxtBox.BackColor = System.Drawing.Color.White;
            this.encryptTxtBox.Enabled = false;
            this.encryptTxtBox.Location = new System.Drawing.Point(28, 186);
            this.encryptTxtBox.Multiline = true;
            this.encryptTxtBox.Name = "encryptTxtBox";
            this.encryptTxtBox.Size = new System.Drawing.Size(629, 86);
            this.encryptTxtBox.TabIndex = 5;
            this.encryptTxtBox.TextChanged += new System.EventHandler(this.EncryptTxtBox_TextChanged);
            // 
            // decryptLbl
            // 
            this.decryptLbl.AutoSize = true;
            this.decryptLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.decryptLbl.Location = new System.Drawing.Point(34, 163);
            this.decryptLbl.Name = "decryptLbl";
            this.decryptLbl.Size = new System.Drawing.Size(287, 20);
            this.decryptLbl.TabIndex = 6;
            this.decryptLbl.Text = "Сообщение в зашифрованном виде:";
            // 
            // typeEncryptLbl
            // 
            this.typeEncryptLbl.AutoSize = true;
            this.typeEncryptLbl.Location = new System.Drawing.Point(679, 29);
            this.typeEncryptLbl.Name = "typeEncryptLbl";
            this.typeEncryptLbl.Size = new System.Drawing.Size(93, 13);
            this.typeEncryptLbl.TabIndex = 7;
            this.typeEncryptLbl.Text = "Тип шифрования";
            // 
            // sendMsgBtn
            // 
            this.sendMsgBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.sendMsgBtn.Enabled = false;
            this.sendMsgBtn.Image = ((System.Drawing.Image)(resources.GetObject("sendMsgBtn.Image")));
            this.sendMsgBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.sendMsgBtn.Location = new System.Drawing.Point(669, 229);
            this.sendMsgBtn.Name = "sendMsgBtn";
            this.sendMsgBtn.Size = new System.Drawing.Size(120, 43);
            this.sendMsgBtn.TabIndex = 8;
            this.sendMsgBtn.Text = "Отправить";
            this.sendMsgBtn.UseVisualStyleBackColor = true;
            this.sendMsgBtn.Click += new System.EventHandler(this.SendMsgBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 14F);
            this.label1.Location = new System.Drawing.Point(699, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 22);
            this.label1.TabIndex = 14;
            this.label1.Text = "RSA";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.textBox3);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.textBox2);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Location = new System.Drawing.Point(28, 297);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(765, 311);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Служебная информация";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 206);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(28, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Хэш";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(12, 225);
            this.textBox3.Multiline = true;
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(741, 66);
            this.textBox3.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 111);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Открытый ключ";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(9, 130);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(741, 66);
            this.textBox2.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Закрытый ключ";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(9, 38);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(741, 66);
            this.textBox1.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(800, 619);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.sendMsgBtn);
            this.Controls.Add(this.typeEncryptLbl);
            this.Controls.Add(this.decryptLbl);
            this.Controls.Add(this.encryptTxtBox);
            this.Controls.Add(this.encryptBtn);
            this.Controls.Add(this.originalTxtBox);
            this.Controls.Add(this.originalLbl);
            this.DoubleBuffered = true;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Отправитель";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label originalLbl;
        private System.Windows.Forms.TextBox originalTxtBox;
        private System.Windows.Forms.Button encryptBtn;
        private System.Windows.Forms.TextBox encryptTxtBox;
        private System.Windows.Forms.Label decryptLbl;
        private System.Windows.Forms.Label typeEncryptLbl;
        private System.Windows.Forms.Button sendMsgBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox1;
    }
}

